#include <gtest/gtest.h>

#include "UObject.h"
#include "interfaces.h"

TEST(Fuelable, normal)
{
    UObject *tank = new UObject();
    FuelableImpl<int> *fuelable = new FuelableImpl<int>(*tank);
    fuelable->setFuelLevel(100);
    ASSERT_EQ(fuelable->getFuelLevel(), 100);

    fuelable->setFuelLevel(90);
    ASSERT_EQ(fuelable->getFuelLevel(), 90);
}