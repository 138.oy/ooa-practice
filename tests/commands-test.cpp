#include <gtest/gtest.h>

#include "UObject.h"
#include "interfaces.h"
#include "commands.h"
#include "resolve.h"

#include <iostream>

TEST(BurnFuelCommand, normal)
{
    UObject *tank = new UObject();
    FuelableImpl<int> *fuelable = new FuelableImpl<int>(*tank);
    resolve("fuel_level", *tank, 100);

    BurnFuelCommand *bf_command = new BurnFuelCommand(*fuelable, 10);

    std::cout << resolve<int>("fuel_level", *tank) << std::endl;

    bf_command->Execute();
    std::cout << resolve<int>("fuel_level", *tank) << std::endl;

    bf_command->Execute();
    std::cout << resolve<int>("fuel_level", *tank) << std::endl;

    bf_command->Execute();
    std::cout << resolve<int>("fuel_level", *tank) << std::endl;

    bf_command->Execute();
    std::cout << resolve<int>("fuel_level", *tank) << std::endl;

    bf_command->Execute();
    std::cout << resolve<int>("fuel_level", *tank) << std::endl;

    bf_command->Execute();
    std::cout << resolve<int>("fuel_level", *tank) << std::endl;
}