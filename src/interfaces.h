#pragma once

#include "UObject.h"
#include "resolve.h"

template <typename T>
class Movable
{
public:
    virtual T getPosition() const = 0;
    virtual void setPosition(T const &newValue) = 0;
    virtual T getVelocity() const = 0;
    virtual ~Movable() = default;
};

template <typename T>
class Fuelable
{
public:
    virtual T getFuelLevel() const = 0;
    virtual void setFuelLevel(T const &newValue) = 0;
    virtual ~Fuelable() = default;
};

template <typename T>
class FuelableImpl : public Fuelable<T>
{
private:
    UObject &m_obj;

public:
    FuelableImpl(UObject &obj) : m_obj(obj)
    {
    }
    T getFuelLevel() const
    {
        return resolve<T>("fuel_level", m_obj);
    };
    void setFuelLevel(T const &newValue)
    {
        resolve("fuel_level", m_obj, newValue);
    };
    ~FuelableImpl() {}
};