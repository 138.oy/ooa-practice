#pragma once

#include "interfaces.h"
#include <queue>
class Command
{
public:
    virtual void Execute() = 0;
    virtual ~Command() = default;
};

class BurnFuelCommand : public Command
{
private:
    Fuelable<int> &m_fuelable;
    int m_to_burn;

public:
    BurnFuelCommand(Fuelable<int> &fuelable, int to_burn) : m_fuelable(fuelable), m_to_burn(to_burn)
    {
    }

    void Execute()
    {
        int current_level = m_fuelable.getFuelLevel();
        m_fuelable.setFuelLevel(current_level - m_to_burn);
    }

    ~BurnFuelCommand() {}
};

template <class Vector>
class MoveCommand: public Command {
    Movable<Vector> &m_obj;
    const Vector m_newPos;
public:
    MoveCommand(Movable<Vector> &obj, const Vector& newPos):
            m_obj(obj), m_newPos(newPos){}

    void Execute() {
        m_obj.setPosition(m_newPos);
    }
};

