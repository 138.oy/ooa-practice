#pragma once

#include "UObject.h"

template <typename T>
T resolve(const std::string &key, UObject &obj)
{
    return std::any_cast<T>(obj[key]);
}

template <typename T>
void resolve(const std::string &key, UObject &obj, T val)
{
    obj[key] = val;
}