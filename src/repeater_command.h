//
// Created by Ivan on 11.03.2021.
//

#pragma once
#include "macro_command.h"
#include "commands.h"
class RepeaterCommand: public Command{
    int max_count = 0;
    int counter = 0;
    MacroCommand &shell;
public:
    RepeaterCommand(int _counter, MacroCommand &_shell): counter(_counter), shell(_shell){}

    void Execute() {
        if (counter <= max_count){
            shell.Execute();
        }
        counter++;
    }
};