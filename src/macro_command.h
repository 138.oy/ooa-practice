//
// Created by Ivan on 11.03.2021.
#pragma once

#include <queue>
#include "command.h"
class MacroCommand : public Command{
    std::queue<Command> queue;
public:
    void Execute() {
        std::queue<Command> active = queue;
        while (!active.empty()){
            active.front().Execute();
            active.pop();
        }
    }

    void addCommand(Command command) {
        queue.push(command);
    }
};

